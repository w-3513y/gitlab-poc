# GitLab

this is a project to test the use of gitlab CI/CD

## Tecnologies

here the tecnologies that was implemented:

Back:
- dotnet 6.0 and csharp 10
- Minimal API
- JsonSerializer (to simulate a database access)

Front:
- Angular/CLI 13.3.3
- npm 8.6.0
- Node 16.14.2
- TypeScript 4.6.3

Devops:
- Git
- GitLab
- GitLab CI/CD

Tools:
- VS Code (actually the back-end debug is set by the launch file)

## Installation

to run and debug the front-end project:
* download npm and install, after this:

```bash
npm install 
npm install -g @angular/cli@13.3.3
npm install -g typescript@4.6.3
```

to run and debug back-end project:
* download dotnet-sdk-6.0 and install

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Project status
project created, now we are going to introduce gitlab yaml

# Changelog
## [1.0.0] - 2022-06-12
### Added
- Included a Minimal API with dotnet 6.0
- Included an User Interface with Angular
- Included back-end tests
- update back-end



