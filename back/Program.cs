using repositories;

var builder = WebApplication.CreateBuilder(args);

 builder.Services.AddCors();

Environment.SetEnvironmentVariable("gamesCollection", @"./data/games.json");
Environment.SetEnvironmentVariable("championshipCollection", @"./data/championship.json");


var app = builder.Build();

app.UseCors(opcoes => opcoes.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

app.MapGet("/table", async () =>
    await Task.FromResult(Table.Get().OrderByDescending(t => (t.won * 3 + t.draw)).ThenByDescending(t => (t.goalsfor - t.goalsagainst)).ThenBy(t => t.team)));

app.MapGet("/teams", async () =>
    await Task.FromResult(Table.Get().Select(t => new TeamViewModel() { name = t.team })));    

app.MapPost("/game", async (GamesModel game) =>
    await Task.FromResult(Games.Post(game)));

app.Run();
