using System.Text.Json;

namespace repositories;

public static class Table{

    public static IEnumerable<TableViewModel> Get(){
        using (StreamReader sr = new(Environment.GetEnvironmentVariable("championshipCollection")))
        {
            return JsonSerializer.Deserialize<IEnumerable<TableViewModel>>(sr.ReadToEnd());
        }
    }
}

public class TableViewModel{
        public string? team  {get; set;}
        public int won {get; set;}
        public int lost {get; set;}
        public int draw {get; set;}
        public int goalsfor {get; set;}
        public int goalsagainst {get; set;}
}

public class TeamViewModel{
    public string name { get; set; }
}
