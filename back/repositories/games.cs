using System.Text.Json;

namespace repositories;

public class Games{

    public static IEnumerable<GamesModel> Game(){
        using (StreamReader sr = new(Environment.GetEnvironmentVariable("gamesCollection")))
        {
            return JsonSerializer.Deserialize<IEnumerable<GamesModel>>(sr.ReadToEnd());
        }
    }

    public static string Post(GamesModel game){
        List<GamesModel> games = Game().ToList();
        games.Add(game);
        UpdateTable(game);
        File.WriteAllText(@".\data\games.json", JsonSerializer.Serialize(games));
        return "game included";
    } 

    private static void UpdateTable (GamesModel game){
        var table = Table.Get();
        var tableUpdate = table.Select(t =>
        {
            return new TableViewModel()
            {
                team = t.team,
                won = winner(game, t.team) ? ++t.won : t.won,
                lost = looser(game, t.team) ? ++t.lost : t.lost,
                draw = new [] { game.teamhome, game.teamaway }.Contains(t.team) && game.goalshome == game.goalsaway ? ++t.draw : t.draw,
                goalsfor = t.goalsfor + goalsfor(game, t.team),
                goalsagainst = t.goalsagainst + goalsagainst(game, t.team)};
        }).ToList();
        File.WriteAllText(@".\data\championship.json", JsonSerializer.Serialize(tableUpdate));
    }

    private static bool winner(GamesModel game, string team) =>
        (team == game.teamhome && game.goalshome > game.goalsaway) ||
        (team == game.teamaway && game.goalsaway > game.goalshome);

    private static bool looser(GamesModel game, string team) =>
        (team == game.teamhome && game.goalshome < game.goalsaway) ||
        (team == game.teamaway && game.goalsaway < game.goalshome);

    private static int goalsfor(GamesModel game, string team){
        if (game.teamhome == team) return game.goalshome;
        else if (game.teamaway == team) return game.goalsaway;
        else return 0;
    }

    private static int goalsagainst(GamesModel game, string team){
        if (game.teamhome == team) return game.goalsaway;
        else if (game.teamaway == team) return game.goalshome;
        else return 0;
    }    
}

public class GamesModel{
        public string? teamhome {get; set;}
        public string? teamaway {get; set;}
        public int goalshome {get; set;}
        public int goalsaway {get; set;}
}
