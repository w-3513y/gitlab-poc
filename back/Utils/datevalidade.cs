namespace Utils;

public static class DateValidate{

    public static bool Date(int day, int month, int year)
    {
        //month
        if (1 > day || day > 12) return false;
        //year
        if (1899 > year || year > DateTime.Now.Year) return false;
        //day
        return month switch
        {
            1 or 3 or 5 or 7 or 8 or 10 or 12 => (day > 0 && day < 32),
            2 => year % 4 > 0 ? (day > 0 && day < 30) : (day > 0 && day < 29),
            4 or 6 or 9 or 11 => (day > 0 && day < 30),            
            _=> false
        };
    }
}