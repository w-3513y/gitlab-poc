import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { TableViewModel } from '../models/table-view-model';
import { environment } from 'src/environments/environment';

const prefix = `${environment.apiUrl}`;

@Injectable({
  providedIn: 'root'
})

export class TableService {

  constructor(
    private _httpClient: HttpClient
  ) { }

  getAll(): Observable<TableViewModel[]>{
    return this._httpClient.get<TableViewModel[]>(
      `${prefix}/table`
    )
  }
}
