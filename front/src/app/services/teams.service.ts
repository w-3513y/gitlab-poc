import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { TeamViewModel } from '../models/team-view-model';
import { environment } from 'src/environments/environment';
import { GameViewModel } from '../models/game-view-model';

const prefix = `${environment.apiUrl}`;
const options = {
  responseType: 'text' as const, 
}

@Injectable({
  providedIn: 'root'
})

export class TeamsService {

  constructor(
    private _http: HttpClient
  ) { }

  getAll(): Observable<TeamViewModel[]>{
    return this._http.get<TeamViewModel[]>(`${prefix}/teams`)
  }

  insert(game: GameViewModel): Observable<string>{
    return this._http.post(`${prefix}/game`, game, options);
  }
}
