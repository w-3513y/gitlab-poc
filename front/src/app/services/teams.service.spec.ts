import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { GameViewModel } from '../models/game-view-model';
import { TeamViewModel } from '../models/team-view-model';

import { TeamsService } from './teams.service';

describe('TeamsService', () => {
  let service: TeamsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    TestBed.configureTestingModule({
      providers: [
        TeamsService,
        { provide: HttpClient, useValue: spy},
      ],
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(TeamsService);

    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all', () => {
    const team: TeamViewModel[] = [{"name": "example"}]
    httpClientSpy.get.and.returnValue(of(team));

    service.getAll().subscribe((v) => expect(v).toBe(team));
  })

  it('should insert a game', () => {
    const gameIncluded = 'game included';
    const game: GameViewModel = {"teamhome": "home", "teamaway": "away", "goalshome": 0, "goalsaway": 0};
    httpClientSpy.post.and.returnValue(of(gameIncluded));

    service.insert(game).subscribe((v) => expect(v).toBe(gameIncluded));
  })
});
