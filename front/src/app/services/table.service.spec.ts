import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { TableViewModel } from '../models/table-view-model';

import { TableService } from './table.service';

describe('TableService', () => {
  let service: TableService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);

    TestBed.configureTestingModule({
      providers: [
        TableService,
        { provide: HttpClient, useValue: spy},
      ],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(TableService);

    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all', () => {
    const table: TableViewModel[] = [{"team": "example", "won": 1, "draw": 0, "lost": 2, "goalsfor": 5, "goalsagainst": 7}]
    httpClientSpy.get.and.returnValue(of(table));

    service.getAll().subscribe((v) => expect(v).toBe(table));
  })
});
