import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TableService } from 'src/app/services/table.service';

import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let testServiceSpy: jasmine.SpyObj<TableService>;
  let nativeElement: HTMLElement;

  beforeEach(async () => {
    testServiceSpy = jasmine.createSpyObj('TableService', [
      'getAll',
    ]);
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ TableComponent ],
      providers: [{ provide: TableService }],
    })
    .compileComponents();


    //testServiceSpy = TestBed.inject(TableService) as jasmine.SpyObj<TableService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
