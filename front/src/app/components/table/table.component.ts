import { Component, OnInit } from '@angular/core';
import { TableService } from 'src/app/services/table.service';
import { TableViewModel } from '../../models/table-view-model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  
  table: TableViewModel[] = []

  constructor(
    private _tableService: TableService
  ) { }

  ngOnInit(): void {
    this.getTableObserver()
  }

  private getTableObserver(): void {
    this._tableService.getAll().subscribe(result => this.table = result)
  }

}
