import { Component, OnInit } from '@angular/core';
import { GameViewModel } from 'src/app/models/game-view-model';
import { TeamsService } from 'src/app/services/teams.service';
import { TeamViewModel } from '../../models/team-view-model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  teams: TeamViewModel[] = []
  game: GameViewModel = {"teamhome": "", "teamaway": "", "goalshome": 0, "goalsaway": 0};

  constructor(
    private _teamsService: TeamsService
  ) { }

  ngOnInit(): void {
    this.getTeamObserver()
  }

  private getTeamObserver(): void {
    this._teamsService.getAll().subscribe(result => this.teams = result)
  }

  submitGame(): void {
    console.log(this.game)
    this._teamsService.insert(this.game).subscribe(result => console.log(result))
  }
}
