import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { GameViewModel } from 'src/app/models/game-view-model';
import { TeamsService } from 'src/app/services/teams.service';

import { GameComponent } from './game.component';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let testServiceSpy: jasmine.SpyObj<TeamsService>;
  let nativeElement: HTMLElement;
  

  beforeEach(async () => {
    testServiceSpy = jasmine.createSpyObj('TeamsService', [
      'getAll', 'insert'
    ]);
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ GameComponent ],
      providers: [{ provide: TeamsService }],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit a game', () => {
    testServiceSpy.insert.and.callFake(() => of("game submited"));
    const button = nativeElement.querySelector('.btn-outline-primary');
    button?.dispatchEvent(new Event('click'));
    expect(button).toBeDefined();
  })


});
