export interface TableViewModel{
    team: string;
    won: number;
    draw: number;
    lost: number;
    goalsfor: number;
    goalsagainst: number;
}