export interface GameViewModel{
     teamhome: string;
     teamaway: string;
     goalshome: number;
     goalsaway: number;
}