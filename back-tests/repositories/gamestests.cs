using Microsoft.VisualStudio.TestTools.UnitTesting;
using repositories;
using System;
using System.IO;
using System.Linq;

namespace GitLabBackTests.repositories;

[TestClass]
public class GamesTest
{   
    [TestInitialize]
    public void Startup(){
        var projectDirectory = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.FullName ?? "";
        var systemOp =  Environment.GetEnvironmentVariable("OS")?.Equals("Windows_NT") ?? false;
        if (systemOp){
            Environment.SetEnvironmentVariable("gamesCollection", $@"{projectDirectory}\data\games.json");
            Environment.SetEnvironmentVariable("championshipCollection", $@"{projectDirectory}\data\championship.json");
        }
        else 
        {
            Environment.SetEnvironmentVariable("gamesCollection", $@"{projectDirectory}/data/games.json");
            Environment.SetEnvironmentVariable("championshipCollection", $@"{projectDirectory}/data/championship.json");
        }
    }
    
    [TestMethod]
    public void Game(){
        //act
        var game = Games.Game();
        //assert
        Assert.AreEqual(0, game.ToList().Count());
    }

    [TestMethod]
    public void Post(){
        //act
        var result = Games.Post(new GamesModel() { teamhome = "Arsenal", teamaway = "Chelsea", goalshome = 1, goalsaway = 0 });
        //assert
        Assert.AreEqual("game included", result);
    } 

    [TestMethod]
    public void PostDraw(){
        //act
        var result = Games.Post(new GamesModel() { teamhome = "Arsenal", teamaway = "Chelsea", goalshome = 0, goalsaway = 0 });
        //assert
        Assert.AreEqual("game included", result);
    } 
}