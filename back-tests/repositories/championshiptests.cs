using Microsoft.VisualStudio.TestTools.UnitTesting;
using repositories;
using System;
using System.IO;
using System.Linq;

namespace GitLabBackTests.repositories;

[TestClass]
public class ChampionshipTest
{
    [TestInitialize]
    public void Startup(){
        var projectDirectory = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.FullName ?? "";
        var systemOp =  Environment.GetEnvironmentVariable("OS")?.Equals("Windows_NT") ?? false;
        if (systemOp){
            Environment.SetEnvironmentVariable("gamesCollection", $@"{projectDirectory}\data\games.json");
            Environment.SetEnvironmentVariable("championshipCollection", $@"{projectDirectory}\data\championship.json");
        }
        else {
            Environment.SetEnvironmentVariable("gamesCollection", $@"{projectDirectory}/data/games.json");
            Environment.SetEnvironmentVariable("championshipCollection", $@"{projectDirectory}/data/championship.json");
        }
    }

    [TestMethod]
    public void Get(){
        //act
        var team = Table.Get();
        //assert
        Assert.AreEqual(10, team.ToList().Count());
    }

    [TestMethod]
    public void GetTeams(){
        //act
        var team = Table.Get().Select(t => new TeamViewModel() { name = t.team });
        //assert
        Assert.AreEqual(10, team.ToList().Count());
    }

    [TestMethod]
    public void GetWinner(){
        //act
        var team = Table.Get().OrderByDescending(t => (t.won * 3 + t.draw)).ThenByDescending(t => (t.goalsfor - t.goalsagainst)).ThenBy(t => t.team).FirstOrDefault();
        //assert
        Assert.AreEqual("Chelsea", team.team);
    }
}
